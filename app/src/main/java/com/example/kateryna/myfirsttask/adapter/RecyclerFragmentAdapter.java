package com.example.kateryna.myfirsttask.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kateryna.myfirsttask.DateUtils;
import com.example.kateryna.myfirsttask.DetailsActivity;
import com.example.kateryna.myfirsttask.POJO.Address;
import com.example.kateryna.myfirsttask.POJO.Model;
import com.example.kateryna.myfirsttask.R;

import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class RecyclerFragmentAdapter extends RecyclerView.Adapter<RecyclerFragmentAdapter.ViewHolder> {
    private Context mContext;
    private List<Model> mData;

    @InjectView(R.id.tab_recycler_item_pattern)
    View mTabRecyclerView;

    public RecyclerFragmentAdapter(List<Model> newData) {
        this.mData = newData;
    }

    /**
     * Usually involves inflating a layout from XML and returning the holder
     *
     * @param parent   parent view
     * @param viewType parent view type
     * @return
     */
    @Override
    public RecyclerFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mTabRecyclerView = inflater.inflate(R.layout.tab_recycler_item, parent, false);
        ButterKnife.inject(this, mTabRecyclerView);

        return new ViewHolder(mTabRecyclerView);
    }

    /**
     * Involves populating mData into the item through holder
     *
     * @param viewHolder instance of class that provides access to view
     * @param position   of item in recycler view
     */
    @Override
    public void onBindViewHolder(RecyclerFragmentAdapter.ViewHolder viewHolder, int position) {
        if (mData.isEmpty()) {
            return;
        }
        final Model model = mData.get(position);

        viewHolder.getActivityIcon().setBackgroundResource(R.mipmap.ic_doc);
        viewHolder.getThumbIcon().setColorFilter(mContext.getResources().getColor(R.color.LightGray));
        viewHolder.getActivityDescription().setText(model.getCategory().getName());
        viewHolder.getLikesQuantity().setText(String.valueOf(model.getLikesCounter()));Address address = model.getUser().getAddress();
        String streetType = address.getStreet().getStreetType().getShortName();
        if (streetType.equals("-")) {
            streetType = "";
        }
        Locale currentLocale = mContext.getResources().getConfiguration().locale;
        viewHolder.getAddress().setText(streetType + " "
                + address.getStreet().getLocalizedName(currentLocale) + ", "
                + address.getHouse().getName() + ", "
                + address.getCity().getLocalizedName(currentLocale));

        String ifEmptyValue = mContext.getResources().getString(R.string.unknown);
        viewHolder.getDate().setText(DateUtils.convertDate(model.getCreatedDate(), ifEmptyValue));
        viewHolder.getTerm().setText(getDaysLeft(model, ifEmptyValue));

        mTabRecyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtra("cardId", String.valueOf(model.getId()));
                mContext.startActivity(intent);
            }
        });
    }

    private String getDaysLeft(Model model, String ifEmptyValue) {
        if (model.getDeadline() == null || model.getDeadline() == 0) {
            return ifEmptyValue;
        }
        int secondsLeft = model.getDeadline() - model.getStartDate();
        return (secondsLeft / DateUtils.SECONDS_IN_DAY) + " " +
                mContext.getResources().getString(R.string.days);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    /**
     * Provides a direct reference to each of the views within a mData item
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView activityIcon;
        private ImageView thumbIcon;
        private TextView likesQuantity;
        private TextView activityDescription;
        private TextView address;
        private TextView date;
        private TextView term;

        /**
         * Stores all views in a private member variables that can be used
         * to access the mContext from any ViewHolder instance.
         *
         * @param itemView for views
         */
        public ViewHolder(View itemView) {
            super(itemView);
            activityIcon = ButterKnife.findById(itemView, R.id.activity_icon);
            thumbIcon = ButterKnife.findById(itemView, R.id.icon_thumb);

            likesQuantity = ButterKnife.findById(itemView, R.id.likes_quantity);
            activityDescription = ButterKnife.findById(itemView, R.id.activity_description);
            address = ButterKnife.findById(itemView, R.id.address);
            date = ButterKnife.findById(itemView, R.id.date);
            term = ButterKnife.findById(itemView, R.id.term);
        }

        public ImageView getActivityIcon() {
            return activityIcon;
        }

        public ImageView getThumbIcon() {
            return thumbIcon;
        }

        public TextView getLikesQuantity() {
            return likesQuantity;
        }

        public TextView getActivityDescription() {
            return activityDescription;
        }

        public TextView getAddress() {
            return address;
        }

        public TextView getDate() {
            return date;
        }

        public TextView getTerm() {
            return term;
        }
    }

}
