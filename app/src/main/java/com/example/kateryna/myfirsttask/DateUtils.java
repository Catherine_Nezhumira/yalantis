package com.example.kateryna.myfirsttask;

import java.text.DateFormatSymbols;
import java.util.Calendar;

/**
 * Created by kateryna on 30.05.16.
 */
public final class DateUtils {
    public static final int SECONDS_IN_DAY = 24 * 60 * 60;

    private DateUtils() {}

    public static String convertDate(Integer date, String unknown) {
        return convertDate(Long.valueOf(date), unknown);
    }

    public static String convertDate(Long date, String unknown) {
        if (date == null || date == 0) {
            return unknown;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date * 1000);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        return day + " " + DateFormatSymbols.getInstance().getMonths()[month] + " " + year;
    }
}
