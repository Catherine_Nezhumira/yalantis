package com.example.kateryna.myfirsttask.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kateryna.myfirsttask.API;
import com.example.kateryna.myfirsttask.POJO.Model;
import com.example.kateryna.myfirsttask.R;
import com.example.kateryna.myfirsttask.RestApi;
import com.example.kateryna.myfirsttask.adapter.RecyclerFragmentAdapter;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RecyclerFragment extends Fragment {
    private int mPastVisibleItems = 0;
    private int mVisibleItemCount = 0;
    private int mTotalItemCount = 0;
    private String[] mStates;
    private Realm mRealm;
    private List<Model> mModels;
    private static final int NEW_ITEMS_AMOUNT = 10;
    private List<Model> mNewData = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_recycler_fragment, container, false);
        ButterKnife.inject(this, rootView);
        initializeRealmDB();
        mStates = getArguments().getStringArray("states");

        initializeRecycler(rootView);
        return rootView;
    }

    private void initializeRecycler(View view) {
        final RecyclerView recyclerView = ButterKnife.findById(view, R.id.tab_fragment_recycler);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        mNewData = loadNewData();
        final RecyclerFragmentAdapter adapter = new RecyclerFragmentAdapter(mNewData);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //checking for scrolling down
                if (dy > 0) {
                    mVisibleItemCount = layoutManager.getChildCount();
                    mTotalItemCount = layoutManager.getItemCount();
                    mPastVisibleItems = layoutManager.findFirstVisibleItemPosition();

                    if ((mVisibleItemCount + mPastVisibleItems) >= mTotalItemCount) {
                        //Doing pagination
                        mNewData = loadNewData();
                        adapter.notifyItemInserted(mNewData.size() - 1);
                    }
                }
            }
        });
    }

    private void initializeRealmDB() {
        this.mRealm = Realm.getInstance(new RealmConfiguration.Builder(getActivity().getApplicationContext()).build());
    }

    private List<Model> loadNewData() {
        if (checkForDataExisting(mStates).size() < (mTotalItemCount + NEW_ITEMS_AMOUNT)) {
            loadNextItems(new String[]{Arrays.toString(mStates),
                    String.valueOf(NEW_ITEMS_AMOUNT), String.valueOf(mTotalItemCount)});
            writeDataToDB(mModels);
        }
        return readDataFromDB();
    }

    private List<Model> checkForDataExisting(String[] states) {
        RealmQuery<Model> query = mRealm.where(Model.class);
        query.equalTo("state.id", Integer.valueOf(states[0]));
        for (int currentId = 1; currentId < states.length; currentId++) {
            query.or().equalTo("state.id", Integer.valueOf(states[currentId]));
        }

        return query.findAll();
    }

    private void loadNextItems(String[] urlParameters) {
        LongOperation longOperation = new LongOperation();
        longOperation.execute(urlParameters);
        try {
            longOperation.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private class LongOperation extends AsyncTask<String, Void, List<Model>> {
        @Override
        protected List<Model> doInBackground(String... params) {
            Gson gson = new GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .create();
            //Creating Rest Services
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API.SERVER_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            RestApi service = retrofit.create(RestApi.class);
            Call<List<Model>> call = service.getReport(params[0], params[1], params[2]);

            //Executing Call
            try {
                mModels = call.execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return mModels;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(List<Model> result) {
        }
    }

    private void writeDataToDB(List<Model> response) {
        for (int currentEntry = 0; currentEntry < response.size(); currentEntry++) {
            mRealm.beginTransaction();
            mRealm.copyToRealmOrUpdate(response.get(currentEntry));
            mRealm.commitTransaction();
        }
    }

    private List<Model> readDataFromDB() {
        RealmQuery<Model> query = mRealm.where(Model.class);
        query.equalTo("state.id", Integer.valueOf(mStates[0]));
        for (int currentId = 1; currentId < mStates.length; currentId++) {
            query.or().equalTo("state.id", Integer.valueOf(mStates[currentId]));
        }

        return query.findAll();
    }
}
