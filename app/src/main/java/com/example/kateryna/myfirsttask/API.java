package com.example.kateryna.myfirsttask;

/**
 * Created by kateryna on 30.05.16.
 */
public final class API {

    public final static String SERVER_URL = "http://dev-contact.yalantis.com";

    public static String getImageUrl(String imageName) {
        return "http://dev-contact.yalantis.com/files/ticket/" + imageName;
    }

    public static String getUserPhotoUrl(String userId) {
        return "https://graph.facebook.com/" + userId + "/picture?type=large";
    }

}
