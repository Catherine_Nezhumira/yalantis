package com.example.kateryna.myfirsttask.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.kateryna.myfirsttask.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;

public class ImageRecyclerAdapter extends RecyclerView.Adapter<ImageRecyclerAdapter.ViewHolder> {
    private Context mContext;
    private List<String> mUrls;

    public ImageRecyclerAdapter(List<String> mUrls) {
        this.mUrls = mUrls;
    }

    /**
     * Usually involves inflating a layout from XML and returning the holder
     *
     * @param parent   parent view
     * @param viewType parent view type
     * @return
     */
    @Override
    public ImageRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final View imageRecyclerView = inflater.inflate(R.layout.recycler_img_item, parent, false);
        ButterKnife.inject(this, imageRecyclerView);

        return new ViewHolder(imageRecyclerView);
    }

    /**
     * Involves populating data into the item through holder
     *
     * @param viewHolder instance of class that provides access to image view
     * @param position   of item in recycler view
     */
    @Override
    public void onBindViewHolder(ImageRecyclerAdapter.ViewHolder viewHolder, int position) {
        Picasso.with(mContext)
                .load(mUrls.get(position))
                .resize((int) mContext.getResources().getDimension(R.dimen.image_item_width),
                        (int) mContext.getResources().getDimension(R.dimen.image_item_height))
                .into(viewHolder.getImage());
            }

    @Override
    public int getItemCount() {
        return mUrls.size();
    }

    /**
     * Provides a direct reference to each of the views within a data item
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;

        /**
         * Stores the itemView in a private member variable that can be used
         * to access the mContext from any ViewHolder instance.
         *
         * @param itemView for image
         */
        public ViewHolder(View itemView) {
            super(itemView);
            image = ButterKnife.findById(itemView, R.id.imageItem);
        }

        public ImageView getImage() {
            return image;
        }
    }

}