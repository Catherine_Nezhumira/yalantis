package com.example.kateryna.myfirsttask;

import android.content.Intent;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.kateryna.myfirsttask.POJO.FBUser;
import com.example.kateryna.myfirsttask.fragments.MapFragment;
import com.example.kateryna.myfirsttask.fragments.TabFragment;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    @InjectView(R.id.tool_bar)
    Toolbar mToolbar;
    @InjectView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    private Realm mRealm;
    private CallbackManager mCallbackManager;
    private MenuItem mItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeRealmDB();
        initializeFBLogin();
        setContentView(R.layout.navigation_drawer);
        ButterKnife.inject(this);

        initializeToolbar();

        initializeNavigationDrawer();
        setBackgroundGradient();

        insertNewFragment(TabFragment.class);
    }

    private void initializeToolbar() {
        mToolbar.setTitle(R.string.all_requests);
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.mipmap.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void initializeNavigationDrawer() {
        NavigationView navigationView = ButterKnife.findById(this, R.id.navigation_view);
        mItem = navigationView.getMenu().findItem(R.id.view_profile);
        boolean isVisible = isLoggedIn();
        mItem.setVisible(isVisible);
        mItem.setCheckable(isVisible);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        if ((menuItem.getItemId() != R.id.login) && (menuItem.getItemId() !=
                                R.id.view_profile)) {
                            mToolbar.setTitle(menuItem.getTitle());
                        }
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    /**
     * Selecting matching fragment class for inserting it into frame
     *
     * @param menuItem of navigation drawer
     */
    public void selectDrawerItem(MenuItem menuItem) {
        Class fragmentClass;
        switch (menuItem.getItemId()) {
            case R.id.nav_first_fragment:
                fragmentClass = TabFragment.class;
                insertNewFragment(fragmentClass);
                break;
            case R.id.nav_second_fragment:
                fragmentClass = MapFragment.class;
                insertNewFragment(fragmentClass);
                break;
            case R.id.login:
                LoginManager.getInstance().logInWithReadPermissions(this,
                        Arrays.asList("public_profile"));
                break;
            case R.id.view_profile :
                Intent intent = new Intent(this, ScrollingActivity.class);
                intent.putExtra("accessToken", AccessToken.getCurrentAccessToken().getToken());
                this.startActivity(intent);
                break;
            default:
                fragmentClass = TabFragment.class;
                insertNewFragment(fragmentClass);
        }
    }

    /**
     * Inserting the fragment by replacing any existing fragment
     *
     * @param fragmentClass matching menu item from navigation drawer
     */
    private void insertNewFragment(Class fragmentClass) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content, fragment).commit();
    }

    /**
     * Setting background of main content layout
     */
    private void setBackgroundGradient() {
        final LinearLayout contentLayout = ButterKnife.findById(this, R.id.main_content);
        ShapeDrawable.ShaderFactory sf = new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int width, int height) {
                return new LinearGradient(0, 0, 0, contentLayout.getHeight(),
                        new int[]{
                                getResources().getColor(R.color.DullBlue),
                                getResources().getColor(R.color.FadedBlue),
                                getResources().getColor(R.color.Blue),
                                getResources().getColor(R.color.DustyBlue)},
                        new float[]{
                                0, 0.55f, 0.75f, 1},
                        Shader.TileMode.REPEAT);
            }
        };

        PaintDrawable p = new PaintDrawable();
        p.setShape(new RectShape());
        p.setShaderFactory(sf);

        contentLayout.setBackground(p);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.filter) {
            return true;
        }
        if (id == android.R.id.home) {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, getResources().getString(R.string.your_choice) +
                v.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }

    private void initializeRealmDB() {
        mRealm = Realm.getInstance(new RealmConfiguration.Builder(getApplicationContext()).build());
    }

    private void initializeFBLogin() {
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        final AccessToken accessToken = loginResult.getAccessToken();

                        final FBUser fbUser = new FBUser();
                        GraphRequest request = GraphRequest.newMeRequest(accessToken,
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                                        mItem.setVisible(true);
                                        mItem.setCheckable(true);

                                        fbUser.setAccessToken(accessToken.getToken());
                                        fbUser.setId(jsonObject.optString("id"));
                                        fbUser.setName(jsonObject.optString("name"));
                                        fbUser.setGender(jsonObject.optString("gender"));
                                        fbUser.setEmail(jsonObject.optString("email"));
                                        fbUser.setBirthday(jsonObject.optString("birthday"));

                                        mRealm.beginTransaction();
                                        mRealm.copyToRealmOrUpdate(fbUser);
                                        mRealm.commitTransaction();
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, name, email, gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(MainActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(MainActivity.this, exception.getMessage(), Toast.LENGTH_LONG)
                                .show();
                    }
                });
    }

    private boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

}
