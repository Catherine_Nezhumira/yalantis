package com.example.kateryna.myfirsttask.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.example.kateryna.myfirsttask.fragments.RecyclerFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private int mTabsQuantity;
    private static final String[] IN_PROGRESS_STATUSES = new String[]{"0", "9", "5", "7", "8"};
    private static final String[] DONE_STATUSES = new String[]{"10","6"};
    private static final String[] PENDING_STATUSES = new String[]{"1","3","4"};

    public PagerAdapter(FragmentManager fragmentManager, int numberOfTabs, Context context) {
        super(fragmentManager);
        this.mTabsQuantity = numberOfTabs;

    }

    /**
     * Returns fragment matching to current tab position
     *
     * @param position of tab
     * @return fragment
     */
    @Override
    public Fragment getItem(int position) {
        Log.e("method ", "get item in PagerAdapter");
        Log.e("position = ", String.valueOf(position));
        Bundle bundle = new Bundle();
        switch (position) {
            case 0:
                bundle.putStringArray("states", IN_PROGRESS_STATUSES);
                RecyclerFragment firstTab = new RecyclerFragment();
                firstTab.setArguments(bundle);
                return firstTab;
            case 1:
                bundle.putStringArray("states", DONE_STATUSES);
                RecyclerFragment secondTab = new RecyclerFragment();

                secondTab.setArguments(bundle);
                return secondTab;
            case 2:
                bundle.putStringArray("states", PENDING_STATUSES);
                RecyclerFragment thirdTab = new RecyclerFragment();
                thirdTab.setArguments(bundle);
                return thirdTab;
            default:
                bundle.putStringArray("states", IN_PROGRESS_STATUSES);
                RecyclerFragment firstTabDefault = new RecyclerFragment();
                firstTabDefault.setArguments(bundle);
                return firstTabDefault;
        }
    }

    @Override
    public int getCount() {
        return mTabsQuantity;
    }

}

