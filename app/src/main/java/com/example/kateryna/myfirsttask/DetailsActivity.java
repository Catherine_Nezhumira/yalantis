package com.example.kateryna.myfirsttask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kateryna.myfirsttask.POJO.File;
import com.example.kateryna.myfirsttask.POJO.Model;
import com.example.kateryna.myfirsttask.adapter.ImageRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Setting click listeners for status and serial number
     *
     * @param id     of element
     * @param string value to display in toast
     */
    @InjectView(R.id.number)
    TextView number;

    @OnClick(R.id.number)
    protected void numberToast() {
        Toast.makeText(DetailsActivity.this, getResources().getString(R.string.your_choice) +
                        getResources().getString(R.string.number_name),
                Toast.LENGTH_LONG).show();
    }

    @InjectView(R.id.status)
    TextView status;

    @OnClick(R.id.status)
    protected void statusToast() {
        Toast.makeText(DetailsActivity.this, getResources().getString(R.string.your_choice) +
                        getResources().getString(R.string.status_name),
                Toast.LENGTH_LONG).show();
    }

    @InjectView(R.id.created)
    TextView created;
    @InjectView(R.id.registered)
    TextView registered;
    @InjectView(R.id.resolve_to)
    TextView resolveTo;
    @InjectView(R.id.responsible)
    TextView responsible;
    @InjectView(R.id.description)
    TextView description;
    @InjectView(R.id.tool_bar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        Intent intent = getIntent();
        String cardId = intent.getStringExtra("cardId");

        initializeToolbar();
        initializeRecycler(Integer.valueOf(cardId));
    }

    private void initializeToolbar() {
        toolbar.setTitle(R.string.title);
        toolbar.setNavigationIcon(R.mipmap.ic_back);
        setSupportActionBar(toolbar);
    }

    private void initializeRecycler(Integer cardId) {
        RecyclerView recyclerView = ButterKnife.findById(this, R.id.rvImages);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        Model model = readModelFromDB(cardId);
        renderList(model);
        ImageRecyclerAdapter adapter = new ImageRecyclerAdapter(readImagesFromDB(model));
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private Model readModelFromDB(Integer cardId) {
        Realm realm = Realm.getInstance(new RealmConfiguration.Builder(getApplication()
                .getApplicationContext()).build());
        RealmResults<Model> results = realm.where(Model.class).equalTo("id", cardId)
                .findAll();
        return results.get(0);
    }

    private void renderList(Model model) {

        toolbar.setTitle(model.getCategory().getName());
        String ifEmptyValue = getResources().getString(R.string.unknown);

        status.setText(getActualInfo(model.getState().getName()));
        number.setText(getActualInfo(model.getTicketId()));
        created.setText(DateUtils.convertDate(model.getCreatedDate(), ifEmptyValue));
        registered.setText(DateUtils.convertDate(model.getStartDate(), ifEmptyValue));
        resolveTo.setText(DateUtils.convertDate(model.getDeadline(), ifEmptyValue));
        String organization = ifEmptyValue;
        if (!model.getPerformers().isEmpty()) {
            organization = model.getPerformers().get(0).getOrganization();
        }
        responsible.setText(organization);
    }

    private String getActualInfo(String entry) {

        String information = getResources().getString(R.string.unknown);
        if (entry != null && !entry.trim().isEmpty()) {
            information = entry;
        }
        return information;
    }

    private List<String> readImagesFromDB(Model model) {
        List<String> images = new ArrayList<>();
        List<File> files = model.getFiles();
        for (File currentFile : files) {
            images.add(API.getImageUrl(currentFile.getFilename()));
        }
        return images;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, getResources().getString(R.string.your_choice) +
                v.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
    }
}




