package com.example.kateryna.myfirsttask.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kateryna.myfirsttask.R;
import com.example.kateryna.myfirsttask.adapter.PagerAdapter;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class TabFragment extends Fragment {
    @InjectView(R.id.fab)
    FloatingActionButton mFab;
    @OnClick(R.id.fab)
    public void showToast(View view) {
        Snackbar.make(view, R.string.ab_click_message, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_fragment,container,false);
        ButterKnife.inject(this, view);
        initializeTabLayout(view);

        return view;
    }

    private void initializeTabLayout(View view) {
        TabLayout tabLayout = ButterKnife.findById(view, R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.in_progress)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.done)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.not_done)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = ButterKnife.findById(view, R.id.pager);

        PagerAdapter adapter = new PagerAdapter
                (getFragmentManager(), tabLayout.getTabCount(), getActivity().getApplicationContext());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

}
