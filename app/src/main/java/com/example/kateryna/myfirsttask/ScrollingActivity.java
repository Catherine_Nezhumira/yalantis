package com.example.kateryna.myfirsttask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kateryna.myfirsttask.POJO.FBUser;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class ScrollingActivity extends AppCompatActivity {
    @InjectView(R.id.user_name)
    TextView mName;
    @InjectView(R.id.user_gender)
    TextView mGender;
    @InjectView(R.id.user_birthday)
    TextView mBirthday;
    @InjectView(R.id.user_email)
    TextView mEmail;
    @InjectView(R.id.user_profile_photo)
    ImageView mPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        ButterKnife.inject(this);
        Intent intent = getIntent();
        String token = intent.getStringExtra("accessToken");
        renderUserProfile(token);
    }

    private void renderUserProfile(String token) {
        FBUser user = readUserDataFromDB(token);
        mName.setText(user.getName());
        mGender.setText(user.getGender());
        mBirthday.setText(user.getBirthday());
        mEmail.setText(user.getEmail());
        String photoUrl = API.getUserPhotoUrl(user.getId());
        Picasso.with(getApplicationContext())
                .load(photoUrl)
                .resize((int) getResources().getDimension(R.dimen.image_item_width),
                        (int) getResources().getDimension(R.dimen.user_photo_height))
                .into(mPhoto);
    }

    private FBUser readUserDataFromDB(String token) {
        Realm realm = Realm.getInstance(new RealmConfiguration.Builder(getApplication()
                .getApplicationContext()).build());
        RealmResults<FBUser> results = realm.where(FBUser.class).equalTo("accessToken", token)
                .findAll();
        return results.get(0);
    }
}
