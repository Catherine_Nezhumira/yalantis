package com.example.kateryna.myfirsttask.model;

public class ListItem {
    private String activityIcon;
    private String thumbIcon;
    private String likesQuantity;
    private String activityDescription;
    private String address;
    private String date;
    private String term;

    public ListItem(String activityIc, String likes, String activityDescription,
                    String address, String date, String term) {
        this.activityIcon = activityIc;
        this.thumbIcon = "ic_thumb";
        this.likesQuantity = likes;
        this.activityDescription = activityDescription;
        this.address = address;
        this.date = date;
        this.term = term;
    }

    public String getActivityIcon() {
        return activityIcon;
    }

    public void setActivityIcon(String activityIcon) {
        this.activityIcon = activityIcon;
    }

    public String getThumbIcon() {
        return thumbIcon;
    }

    public void setThumbIcon(String thumbIcon) {
        this.thumbIcon = thumbIcon;
    }

    public String getLikesQuantity() {
        return likesQuantity;
    }

    public void setLikesQuantity(String likesQuantity) {
        this.likesQuantity = likesQuantity;
    }

    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

}
