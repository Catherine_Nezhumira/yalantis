package com.example.kateryna.myfirsttask;

import com.example.kateryna.myfirsttask.POJO.Model;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by kateryna on 20.05.16.
 */
public interface RestApi {

    @GET("/rest/v1/tickets?")
    Call<List<Model>> getReport(@Query("state") String state, @Query("amount") String amount,
                                @Query("offset") String offset);
}
